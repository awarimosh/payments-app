export const constants = {
    DEFAULT_TAKE: 20,
    DEFAULT_SKIP: 0,
    PAYMENT_STATUS: ['PENDING', 'COMPLETED', 'REJECTED', 'ERROR']
};
