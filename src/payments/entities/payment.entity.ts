import {
    Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, Generated
} from 'typeorm';
import {constants} from "../../common/contants";

@Entity()
export class Payment {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column({ enum: constants.PAYMENT_STATUS, default: 'pending' })
    status: string;

    @Column({ nullable: true })
    order: string;

    @Column('decimal', { precision: 9, scale: 2 })
    amount: number;

    @Column('decimal', { precision: 5, scale: 2 })
    tax: number;

    @Column()
    date: Date;

    @Column()
    returnURL: string;

    @Column()
    shipname: string;

    @Column({ nullable: true })
    email: string;

    @Column({ nullable: true })
    receipt_url: string;

    @Column({ default: false })
    is_deleted: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}











