import { Controller, Redirect } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { Payment } from './entities/payment.entity';
import { PaymentFindDto } from './dtos/payment-find.dto';
import { PaymentsService } from './payments.service';
import { PaymentUpdateDto } from './dtos/payment-update.dto';

@Controller('payments')
export class PaymentsController {
    constructor(private readonly paymentsService: PaymentsService) {

    }

    @MessagePattern({ cmd: 'payments-create' })
    async create(data: any): Promise<any> {
        return await this.paymentsService.create(data);
    }

    @Redirect()
    @MessagePattern({ cmd: 'payments-backdoor' })
    async backdoor(data: any): Promise<any> {
        return await this.paymentsService.backdoor(data);
    }

    @MessagePattern({ cmd: 'payments-find' })
    async find(data: PaymentFindDto): Promise<any> {
        return await this.paymentsService.find(data);
    }

    @MessagePattern({ cmd: 'payments-find-one' })
    async findOne(data: any): Promise<any> {
        return await this.paymentsService.findOne(data);
    }

    @MessagePattern({ cmd: 'payments-update' })
    async update(data: PaymentUpdateDto): Promise<any> {
        return await this.paymentsService.update(data);
    }
}
