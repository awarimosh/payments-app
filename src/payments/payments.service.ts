import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Payment} from './entities/payment.entity';
import {Repository} from 'typeorm';
import {RpcException} from '@nestjs/microservices';

const moment = require('moment-timezone');

const sha1 = require('sha1');

@Injectable()
export class PaymentsService {
    constructor(@InjectRepository(Payment)
                private readonly paymentRepository: Repository<Payment>) {
    }

    async create(data: any): Promise<Payment> {
        try {
            let model = new Payment();
            if(!data.body.order || data.body.order.length <= 0)
                throw new RpcException('payment order missing');
            model.order = data.body.order;
            model.amount = data.body.amount;
            model.tax = data.body.tax;
            if(data.body.date)
                model.date = data.body.date;
            else
                model.date = new Date();
            model.returnURL = data.body.returnURL;
            model.shipname = data.body.shipname;
            await this.paymentRepository.save(model);
            return await this.fakePayment(model);
        } catch (error) {
            throw new RpcException(error);
        }
    }

    async fakePayment(model:Payment): Promise<Payment>{
        await new Promise(resolve => setTimeout(resolve, 1500));
        let temp = Math.random();
        temp = temp * 1000;
        temp = Math.floor(temp);
        console.log('temp', temp);

        if(temp % 2 === 0){
            // confirmed
            model.status = 'COMPLETED';
        }
        else if(temp % 3 === 0){
            // rejected
            model.status = 'REJECTED';
        }
        else{
            console.log('temp', temp);
            // errored
            model.status = 'ERROR';
        }
        this.paymentRepository.save(model);
        return model;
    }

    async backdoor(data) {
        console.log('data.body -: ', data.body);
        if (data.body) {
            try {
                this.paymentRepository.findOne({
                    where: {id: data.body.payment}
                }).then(async (payment: Payment) => {
                    if (payment) {
                        if(data.body.success)
                            payment.status = data.body.success ? 'COMPLETED': 'REJECTED';
                        else
                            payment.status = 'ERROR';
                        await this.paymentRepository.save(payment);
                        return payment;
                    }
                    else {
                        throw new Error(`Payment with ID ${data.params.id} Not Found`);
                    }
                }).catch((error) => {
                    console.log(error);
                    throw new RpcException(error);
                });
            } catch (error) {
                console.log("update", error);
                throw new RpcException(error);
            }
        }
        else {
            throw new RpcException('Payment Not Found');
        }
    }

    async update(data): Promise<any> {
        try {
            this.paymentRepository.findOne({
                where: {id: data.params.id}
            }).then(async (payment: Payment) => {
                if (payment) {
                    payment = {...payment, ...data.body};
                    await this.paymentRepository.save(payment);
                    return payment;
                }
                else {
                    throw new Error(`Payment with ID ${data.params.id} Not Found`);
                }
            }).catch((error) => {
                console.log(error);
                throw new RpcException(error);
            });
        } catch (error) {
            console.log("update", error);
            throw new RpcException(error);
        }
    }

    async find(data): Promise<{ result: Payment[], total: number, page: number, count: number }> {
        let query = {where: {}};
        if (data.query) {
            if (data.query.take && data.query.skip)
                query = {...query, ...{take: data.query.take, skip: data.query.skip}};
            if (data.query.status)
                query.where = {...query.where, ...{status: data.query.status}};
        }
        const [result, total] = await this.paymentRepository.findAndCount({...query, order: {createdAt: 'DESC'}});
        return {
            result: result,
            total: total,
            page: data.query && data.query.take && data.query.skip ? ((data.query.take * 1 + data.query.skip * 1) / data.query.take) : null,
            count: data.query ? parseInt(data.query.take) : null
        }
    }

    async findOne(data): Promise<Payment> {
        return await this.paymentRepository.findOne({where: {...data.params, license: data.user.license.key}})
    }
}
