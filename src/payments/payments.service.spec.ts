import {Repository} from "typeorm";
import {Payment} from "./entities/payment.entity";
import {Test, TestingModule} from "@nestjs/testing";
import {getRepositoryToken} from "@nestjs/typeorm";
import {PaymentsService} from "./payments.service";
import {RpcException} from '@nestjs/microservices';

export class PaymentsRepositoryFake {
    public create(): void {
    }

    public async update(): Promise<void> {
    }

    public async findOne(): Promise<void> {
    }

    public async find(): Promise<void> {
    }
}

describe('PaymentsService', () => {
    let paymentsService: PaymentsService;
    let paymentsRepository: Repository<Payment>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            providers: [
                PaymentsService,
                {
                    provide: getRepositoryToken(Payment),
                    useClass: PaymentsRepositoryFake,
                },
            ]
        }).compile();

        paymentsService = module.get(PaymentsService);
        paymentsRepository = module.get(getRepositoryToken(Payment));
    });

    describe('creating a payment', () => {
        it('throws an error when no order attached to payment', async () => {
            const data = {body: {order: ''}};
            expect.assertions(2);

            try {
                await paymentsService.create(data);
            } catch (e) {
                expect(e).toBeInstanceOf(RpcException);
                expect(e.message).toBe('payment order missing');
            }
        });
    });
});