import {
    IsNotEmpty, IsUUID, ValidateNested, IsOptional, IsBoolean, IsIn, ValidateIf
} from 'class-validator';
import { Type } from 'class-transformer';

export class Body {
    @IsNotEmpty()
    name: string;
}

export class PaymentUpdateDto {
    @IsNotEmpty()
    @Type(() => Body)
    @ValidateNested({ each: true })
    body: Body;
}