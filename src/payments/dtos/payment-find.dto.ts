import {
    IsNotEmpty, IsDateString, IsUUID, ArrayMinSize, ValidateNested, IsArray, IsOptional,
    IsIn, Min, Max, IsMilitaryTime, IsDate
} from 'class-validator';
import {Type} from 'class-transformer';


export class PaymentFindDto {
    @IsOptional()
    @ValidateNested({each: true})
    query: string;
}