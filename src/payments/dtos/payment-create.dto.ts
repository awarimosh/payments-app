import {
    IsNotEmpty, IsUUID, ValidateNested, IsOptional, IsBoolean, IsIn, IsNumber
} from 'class-validator';
import { Type } from 'class-transformer';

export class Body {

    @IsOptional()
    user: string;

    @IsNotEmpty()
    order: string;

    @IsNotEmpty()
    amount: number;

    @IsNotEmpty()
    tax: number;

    @IsOptional()
    date: Date;

    @IsNotEmpty()
    returnURL: string;

    @IsNotEmpty()
    shipname: string;

    @IsOptional()
    email: string;

    @IsNotEmpty()
    receipt_url: string;

    @IsOptional()
    @IsBoolean()
    is_deleted: boolean;
}

export class PaymentCreateDto {
    @IsNotEmpty()
    @Type(() => Body)
    @ValidateNested({ each: true })
    body: Body;
}