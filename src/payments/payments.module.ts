import {Module} from '@nestjs/common';
import {PaymentsService} from './payments.service';
import {PaymentsController} from './payments.controller';
import {Payment} from './entities/payment.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ClientsModule, Transport} from '@nestjs/microservices';

@Module({
    imports: [
        TypeOrmModule.forFeature([Payment]),
    ],
    providers: [PaymentsService],
    controllers: [PaymentsController],
    exports: [PaymentsService]
})
export class PaymentsModule {
}
